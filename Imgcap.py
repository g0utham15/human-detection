import numpy as np
#import tensorflow as tf
import cv2
import imgdet
import time
from imutils.video import VideoStream
import imutils
import multiprocessing
'''
class DetectorAPI:
	def __init__(self):
		self.path_to_ckpt = path_to_ckpt
		self.detection_graph = tf.Graph()
		with self.detection_graph.as_default():
		od_graph_def = tf.GraphDef()
		with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
		serialized_graph = fid.read()
		od_graph_def.ParseFromString(serialized_graph)
		tf.import_graph_def(od_graph_def, name='')

		self.default_graph = self.detection_graph.as_default()
		self.sess = tf.Session(graph=self.detection_graph)

		# Definite input and output Tensors for detection_graph
		self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
		# Each box represents a part of the image where a particular object was detected.
		self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
		# Each score represent how level of confidence for each of the objects.
		# Score is shown on the result image, together with the class label.
		self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
		self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
		self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
		
	def processFrame(self, image,path_to_ckpt,out_q):
		print('new process started')
		self.path_to_ckpt = path_to_ckpt
		self.detection_graph = tf.Graph()
		with self.detection_graph.as_default():
			od_graph_def = tf.GraphDef()
		with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
			serialized_graph = fid.read()
			od_graph_def.ParseFromString(serialized_graph)
			tf.import_graph_def(od_graph_def, name='')
		self.default_graph = self.detection_graph.as_default()
		self.sess = tf.Session(graph=self.detection_graph)
		# Definite input and output Tensors for detection_graph
		self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
		# Each box represents a part of the image where a particular object was detected.
		self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
		# Each score represent how level of confidence for each of the objects.
		# Score is shown on the result image, together with the class label.
		self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
		self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
		self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
		# Expand dimensions since the trained_model expects images to have shape: [1, None, None, 3]
		image_np_expanded = np.expand_dims(image, axis=0)
		# Actual detection.
		while True:
			start_time = time.time()
			(boxes, scores, classes, num) = self.sess.run(
			[self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
			feed_dict={self.image_tensor: image_np_expanded})
			end_time = time.time()
			print("Elapsed Time:", end_time-start_time)
			im_height, im_width,_ = image.shape
			boxes_list = [None for i in range(boxes.shape[1])]
			for i in range(boxes.shape[1]):
				boxes_list[i] = (int(boxes[0,i,0] * im_height),
				int(boxes[0,i,1]*im_width),
				int(boxes[0,i,2] * im_height),
				int(boxes[0,i,3]*im_width))
			l=[]
			boxes, scores, classes, num=boxes_list, scores[0].tolist(), [int(x) for x in classes[0].tolist()], int(num[0])
			for i in range(len(boxes)):
				# Class 1 represents human
				if classes[i] == 1 and scores[i] > 0.5:
					l.append(boxes[i])
			print('new boxes processed')
			out_q.put(l)
	def close(self):
		self.sess.close()
		self.default_graph.close()
'''
if __name__ == "__main__":
	model_path = 'ssdlite_mobilenet_v2_coco_2018_05_09/frozen_inference_graph.pb'
	q=multiprocessing.Queue()
	x=multiprocessing.Queue()
	p1=multiprocessing.Process(target=imgdet.processFrame,args=(model_path,x,q))
	p1.daemon=True
	p1.start()	
	#usingPiCamera = True
	#frameSize = (1280, 720)
	#vs = VideoStream(src=0, usePiCamera=usingPiCamera, resolution=frameSize,                                            # Initialize mutithreading the video stream.
	#		framerate=60).start()
	vs=cv2.VideoCapture(0)  
	print('new process started')
	time.sleep(5)
	print('processes resumes')
	#cap = cv2.VideoCapture('/home/pi/Downloads/TownCentreXVID.avi')
	#processing=True
	c=0
	box=[(0,0,0,0)]
	fno=0
	g=time.time()
	while True:
		start=time.time()
		r,img = vs.read()
		#blank_image = np.zeros((720,1280,3), np.uint8)
		#img=img+blank_image
		fno+=1
		if q.empty() and c==0:
			x.put(img)
			print('Queue empty, Added image to "X" queue')
			c=1
		if not q.empty():
			print('recieved q')
			box=q.get()
			print('boxes updated')
			c=0
		for i in box:
			cv2.rectangle(img,(i[1],i[0]),(i[3],i[2]),(255,0,0),2)
			
		cv2.imshow('Preview',img)
		print('FPS of camera:',fno/(time.time()-g))
		key = cv2.waitKey(1)
		if key & 0xFF == ord('q'):
			break
	
	
